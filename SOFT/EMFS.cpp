#include "EMFS.h"

bool operator==(SFLD const& lhs, SFLD const& rhs){
	if(lhs.x!=rhs.x){
		return false;
	}
	else if(lhs.y!=rhs.y){
		return false;
	}
	else if(lhs.z!=rhs.z){
		return false;
	}
	else if(lhs.m!=rhs.m){
		return false;
	}
	else if(lhs.e!=rhs.e){
		return false;
	}
	else{
    return true;
	}
}

EMEFS::EMEFS(){//Constructor
}

EMEFS::EMEFS(const EMEFS& emefs){
	GRD=emefs.GRD;//Grid upper coordinate limit

	FS=emefs.FS;//Fields sources
	f=emefs.f;//Frequency of field sources
	w=emefs.w;//Power of field sources
	h=emefs.h;//Phase of field sources

	F=emefs.F;//Scalar Fields
	MPR=emefs.MPR;//Material properties for field computation/

	NCS=emefs.NCS;//Number of compute steps
	CS=emefs.CS;//Compute step

}//Duplicador de clase

EMEFS& EMEFS::operator=(const EMEFS& emefs){
	GRD=emefs.GRD;//Grid upper coordinate limit

	FS=emefs.FS;//Fields sources
	f=emefs.f;//Frequency of field sources
	w=emefs.w;//Power of field sources
	h=emefs.h;//Phase of field sources

	F=emefs.F;//Scalar Fields
	MPR=emefs.MPR;//Material properties for field computation/

	NCS=emefs.NCS;//Number of compute steps
	CS=emefs.CS;//Compute step

	return *this;
} //Igualador de clase

std::vector<SFLD> EMEFS::FCA(double sx, double sy, double sz, double a){//Field computation alg
		const double pi = 3.1415926535897931;
		const double c=299792458000;//mm/s

		double ef=0;//
		double mf=0;//
		std::vector<SFLD> fld(FS.size());

		double tpw=0;
		for(uint n=0;n<FS.size();n++){//Field sources iterator and initializator
			tpw+=w[n];
			fld[n]=(SFLD){sx,sy,sz,0,0};
		}

		for(uint n=0;n<FS.size();n++){//Field sources iterator

			if(w[n]>0.001){
				h[n]=0;/////TODO
			for(int esp=0;esp<FS[n].size();esp++){//nth field emiter points iterator
				double distSQ=pow((FS[n][esp].x-sx),2)+pow((FS[n][esp].y-sy),2)+pow((FS[n][esp].z-sz),2);//Distance squared calculaton
				//std::cout<<"EMEFS::PCF::distSQ="<<distSQ<<std::endl;
				double dps=(f[n]*sqrt(distSQ)/c);//Distance phase shift
				//std::cout<<"EMEFS::PCF::dps="<<dps<<std::endl;
				//double dps=0;//Distance phase shift

				fld[n].e+=(((w[n]/tpw) * cos(f[n]*a + h[n] + 3*dps))/(distSQ))*(GRD.xgs*GRD.ygs*GRD.zgs);//Instantaneous sensitivity //TODO not final expresion
				fld[n].m+=(((w[n]/tpw) * cos(f[n]*a + h[n] + 2*dps))/(distSQ))*(GRD.xgs*GRD.ygs*GRD.zgs);//Instantaneous sensitivity //TODO not final

			}
			}
		}//}

		return fld;
}

std::vector<SFLD> EMEFS::EFCA(double sx, double sy, double sz){//Eficacious field computation alg
		const double pi = 3.1415926535897931;
		const double c=299792458000;//mm/s

		// double TIisense=0;//Timed integal
		// double TIisensm=0;//Timed integral
		std::vector<SFLD> fld(FS.size());

		double tpw=0;
		for(uint n=0;n<FS.size();n++){//Field sources iterator
			tpw+=w[n];
			fld[n]=(SFLD){sx,sy,sz,0,0};
		}

		for(uint n=0;n<FS.size();n++){//Field sources iterator
			if(w[n]>0.001){
				h[n]=0;/////TODO
			for(int esp=0;esp<FS[n].size();esp++){//nth field emiter points iterator
				double distSQ=pow((FS[n][esp].x-sx),2)+pow((FS[n][esp].y-sy),2)+pow((FS[n][esp].z-sz),2);//Distance squared calculaton
				//std::cout<<"EMEFS::PCF::distSQ="<<distSQ<<std::endl;
				double dps=(f[n]*sqrt(distSQ)/c);//Distance phase shift
				//std::cout<<"EMEFS::PCF::dps="<<dps<<std::endl;
				//double dps=0;//Distance phase shift

				//isense+=(((ffs.DCOACO.PWC[actosc[n]]/tpw)*   cos(crf*a + ffs.DCOACO.MH[actosc[n]] + 3*dps   ))/(distSQ))*(GRD.xgs*GRD.ygs*GRD.zgs);//Instantaneous sensitivity //TODO not final expresion
				//isensm+=(((ffs.DCOACO.PWC[actosc[n]]/tpw)*   cos(crf*a + ffs.DCOACO.MH[actosc[n]] + 2*dps   ))/(distSQ))*(GRD.xgs*GRD.ygs*GRD.zgs);//Instantaneous sensitivity //TODO not final

				//TIisense+=(((((ffs.DCOACO.PWC[actosc[n]]/tpw)*   sin( ffs.DCOACO.MH[actosc[n]] + 3*dps +(1/(2*pi))  ))/(distSQ))*(GRD.xgs*GRD.ygs*GRD.zgs))-((((ffs.DCOACO.PWC[actosc[n]]/tpw)*   sin( ffs.DCOACO.MH[actosc[n]] + 3*dps   ))/(distSQ))*(GRD.xgs*GRD.ygs*GRD.zgs)));
				//TIisensm+=(((((ffs.DCOACO.PWC[actosc[n]]/tpw)*   sin( ffs.DCOACO.MH[actosc[n]] + 2*dps +(1/(2*pi))  ))/(distSQ))*(GRD.xgs*GRD.ygs*GRD.zgs))-((((ffs.DCOACO.PWC[actosc[n]]/tpw)*   sin( ffs.DCOACO.MH[actosc[n]] + 2*dps   ))/(distSQ))*(GRD.xgs*GRD.ygs*GRD.zgs)));

				double ITM0=(((w[n]/tpw)* sin( h[n] + 3*dps ))/(distSQ))*(GRD.xgs*GRD.ygs*GRD.zgs);
				double ITMT=(((w[n]/tpw)* sin((1/(2*pi))+h[n] + 3*dps ))/(distSQ))*(GRD.xgs*GRD.ygs*GRD.zgs);
				fld[n].m+=(ITMT-ITM0);

				double ITE0=(((w[n]/tpw)*   sin(h[n] + 2*dps   ))/(distSQ))*(GRD.xgs*GRD.ygs*GRD.zgs);
				double ITET=(((w[n]/tpw)*   sin((1/(2*pi))+h[n] + 2*dps   ))/(distSQ))*(GRD.xgs*GRD.ygs*GRD.zgs);
				fld[n].e+=(ITET-ITE0);

			}
			}
		}

		return fld;
}

void EMEFS::PCF(){//Compute field of a given FFS state
	//std::cout<<"EMEFS::PCF::0"<<std::endl;
	//F.clear();
	//std::cout<<"EMEFS::PCF::3"<<std::endl;
	//print(actosc);

	if(CS>NCS){
		CS=0;
	}

	//std::cout<<"EMEFS::PCF::CS="<<CS<<std::endl;

	//for(double sx=GRD.xll+(GRD.xgs*CS);sx<GRD.xul;sx+=GRD.xgs*NCS){for(double sy=GRD.yll+(GRD.ygs*CS);sy<GRD.yul;sy+=GRD.ygs*NCS){for(double sz=GRD.zll+(GRD.zgs*CS);sz<GRD.zul;sz+=GRD.zgs*NCS){//////////////////// Field point iterator
	int nXG=(GRD.xul-GRD.xll)/GRD.xgs;
	int nYG=(GRD.yul-GRD.yll)/GRD.ygs;
	int nZG=(GRD.zul-GRD.zll)/GRD.zgs;

	if(F.size()<FS.size()){
		F.resize(FS.size(),std::vector<SFLD>(nXG*nYG*nZG));
	}


	for (int i=CS;i<nXG*nYG*nZG;i+=NCS){
		std::vector<int>ids= UiTMi(i, {nXG,nYG,nZG});
		double sx=GRD.xll+GRD.xgs*ids[0];
		double sy=GRD.yll+GRD.ygs*ids[1];
		double sz=GRD.zll+GRD.zgs*ids[2];

		std::vector<SFLD> fld=EFCA(sx, sy, sz);
		for(uint fs=0;fs<fld.size();fs++){
		F[fs][i]=fld[fs];
		}

	//}}
	//std::cout<<"sx-GRD.xul="<<sx<<"-"<<GRD.xul<<std::endl;

	}

	//return SENSFIELD;
	CS++;

	}


void EMEFS::CF(){//Compute field of a given FFS state
	const double pi = 3.1415926535897931;
	const double c=299792458000;//mm/s
	F.clear();
	F.resize(FS.size());
	//std::cout<<"FFSCMP::CMPS::actosc"<<std::endl;
	//print(actosc);
	// std::cout<<"EMEFS::CF::PROPERT"<<std::endl;
	// for(uint n=0;n<FS.size();n++){//Field sources iterator
	// 		std::cout<<"w[n]="<<w[n]<<" f[n]="<<f[n]<<" h[n]="<<h[n]<<std::endl;
	// 	}


	for(double sx=GRD.xll;sx<GRD.xul;sx+=GRD.xgs){for(double sy=GRD.yll;sy<GRD.yul;sy+=GRD.ygs){for(double sz=GRD.zll;sz<GRD.zul;sz+=GRD.zgs){//////////////////// Field point iterator

		//F.push_back(EFCA(sx, sy, sz));
		std::vector<SFLD> fld=EFCA(sx, sy, sz);
		for(uint fs=0;fs<fld.size();fs++){
		F[fs].push_back(fld[fs]);
		}

	}}/*std::cout<<"sx-GRD.xul="<<sx<<"-"<<GRD.xul<<std::endl;*/}

	//return SENSFIELD;

}

void EMEFS::FI(){//Field interpolation

}

SFLD EMEFS::CFI(uint n, uint i){//Compute field interference
	assert(F.size()>n);
	assert(F[n].size()>i);
	SFLD sfld=F[n][i];
	for(uint nn=0;nn<F.size();nn++){
		if(n!=nn){
			float mf=sfld.m+F[nn][i].m;
			if(mf<sfld.m) sfld.m=mf;

			float ef=sfld.e+F[nn][i].e;
			if(ef<sfld.e) sfld.e=ef;
		}
	}
	return sfld;
}

std::vector<SFLD> EMEFS::CFI(uint i){
	assert(F.size()>0);
	assert(F[0].size()>i);
	std::vector<SFLD> sfld;
	for(uint n=0;n<F.size();n++){
		sfld.push_back(CFI(n, i));
	}
	return sfld;
}

std::vector<std::vector<SFLD>> EMEFS::CFI(){//Compute field interference
	assert(F.size()>0);
	std::vector<std::vector<SFLD>> sfld(F.size(),std::vector<SFLD>(F[0].size()));
	for(uint n=0;n<F.size();n++){
	for(uint i=0;i<F[n].size();i++){
		sfld[n][i]=(CFI(n,i));
	}}

	return sfld;

}

std::vector<SFLD> EMEFS::NRM(uint n){//Normalize field F[puntos][fuentes]
	assert(F.size()>n);
	std::vector<SFLD> sfld;

	double maxm=F[n][0].m;
	double minm=F[n][0].m;
	double maxe=F[n][0].e;
	double mine=F[n][0].e;

	for (uint i=1;i<F[n].size();i++){
		if(maxm<F[n][i].m){
			maxm=F[n][i].m;
		}
		if(minm>F[n][i].m){
			minm=F[n][i].m;
		}
		if(maxe<F[n][i].e){
			maxe=F[n][i].e;
		}
		if(mine>F[n][i].e){
			mine=F[n][i].e;
		}
	}

	//std::cout<<"EMS.cpp::NRMFLD::"<<"maxm="<<maxm<<" minm="<<minm<<" maxe="<<maxe<<" mine="<<mine<<std::endl;

	for (uint i=0;i<F[n].size();i++){
		double m,e;
		if(maxm==minm) m=0;
		else m=(F[n][i].m-minm)/(maxm-minm);

		if(maxe==mine) e=0;
		else e=(F[n][i].e-mine)/(maxe-mine);

		sfld.push_back({	F[n][i].x, F[n][i].y, F[n][i].z,
							m, e});

	}

	return sfld;
}

std::vector<std::vector<SFLD>> EMEFS::NRM(){
	assert(F.size()>0);
	assert(F[0].size()>0);
	std::vector<std::vector<SFLD>> sfld;
	for(uint n=0;n<F.size();n++){
	sfld.push_back(NRM(n));
	}

	return sfld;
}

std::vector<SFLD> EMEFS::FAC(){
	assert(F.size()>0);
	assert(F[0].size()>0);
	std::vector<SFLD> sfld;
	for(uint i=0;i<F[0].size();i++){
		sfld.push_back(F[0][i]);
		for(uint n=1;n<F.size();n++){
			sfld[i].m+=F[n][i].m;
			sfld[i].e+=F[n][i].e;
		}

	}
	return sfld;
}

bool CFXC(const SFLD &a, const SFLD &b){//Comparison between x coordinates for sorting porpouses
	return a.x>b.x;
}
bool CFYC(const SFLD &a, const SFLD &b){//Comparison between y coordinates for sorting porpouses
	return a.y>b.y;
}
bool CFZC(const SFLD &a, const SFLD &b){//Comparison between z coordinates for sorting porpouses
	return a.z>b.z;
}
bool CFEC(const SFLD &a, const SFLD &b){//Comparison between e values for sorting porpouses
	return a.e>b.e;
}
bool CFMC(const SFLD &a, const SFLD &b){//Comparison between m values for sorting porpouses
	return a.m>b.m;
}

void FtVA(std::vector<SFLD> field, std::vector<float> & position, std::vector<float> & color ,std::string type, float mult){
    position. clear();
    color. clear();
    for(int i=0;i<field.size();i++){
        position.push_back(field[i].x);
        position.push_back(field[i].y);
        position.push_back(field[i].z);

        if(type=="m"){
            color.push_back(1);//R
            color.push_back(0.475);//G
            color.push_back(0.051);//B
            color.push_back(field[i].m*mult);//A
        }
        else if(type=="e"){
            color.push_back(0.051);
            color.push_back(0.863);
            color.push_back(1);
            color.push_back(field[i].e*mult);
        }

    }
}

std::vector<double> EFV(std::vector<SFLD> field, std::string var){//Extract field vaiable
	std::vector<double> scalv;
	for(int i=0;i<field.size();i++){
		if(var=="m"){
			scalv.push_back(field[i].m);
		}
		else if(var=="e"){
			scalv.push_back(field[i].e);
		}
	}

	return scalv;
}

uint IDBP(std::vector<SFLD> field, double x, double y, double z){//Returns the id of the closest field element to a given position
	assert(field.size()>0);
	uint idbp=0;
	double mind=((field[0].x-x)*(field[0].x-x))+((field[0].y-y)*(field[0].y-y))+((field[0].z-z)*(field[0].z-z));

	//std::cout<<"IDBP::mind="<<mind<<std::endl;

	for(uint f=1;f<field.size();f++){
		double dist=((field[f].x-x)*(field[f].x-x))+((field[f].y-y)*(field[f].y-y))+((field[f].z-z)*(field[f].z-z));
		//std::cout<<"IDBP::mind/dist"<<mind<<"/"<<dist<<std::endl;
		if(dist<mind){
			mind=dist;
			idbp=f;
		}
	}
	return idbp;
}


std::vector <float> SFLDtFV(SFLD sfld){//Scalar field to float vector

	std::vector <float> fv={sfld.x, sfld.y, sfld.z, sfld.m, sfld.e};
	return fv;

}

SFLD FVtSFLD(std::vector <float> fv){////Float vector to scalar field
	assert(fv.size()>=5);
	SFLD sfld={fv[0], fv[1], fv[2], fv[3], fv[4]};
	return sfld;

}

std::vector<std::vector<float>> SFLDVtFV(std::vector<SFLD> sfldv){//Scalar field vector to float vector
	std::vector<std::vector<float>> fv;
	for(int i=0;i<sfldv.size();i++){
		fv.push_back(SFLDtFV(sfldv[i]));
	}
	return fv;

}

std::vector <SFLD> FVtSFLDV(std::vector<std::vector<float>> fv){//Scalar field to float vector
	std::vector<SFLD> sfldv;

	for(int i=0;i<fv.size();i++){
		sfldv.push_back(FVtSFLD(fv[i]));
	}

	return sfldv;

}

std::string FLDTSTR(std::vector<SFLD> field){

	std::string STR;

	for(uint i=0;i<field.size();i++){
		// std::stringstream STRS;
		// STRS << std::fixed << std::setprecision(sigdigits) << fvdata[i][ii];
		// tSTVECT.push_back(STRS.str());
		STR+=std::to_string(field[i].x)+";";
		STR+=std::to_string(field[i].y)+";";
		STR+=std::to_string(field[i].z)+";";
		STR+=std::to_string(field[i].m)+";";
		STR+=std::to_string(field[i].e);

		if(i<field.size()-1) STR+="\n";

	}

	//std::cout<<"FLDTSTR::STR.length()"<<STR.length()<<std::endl;

	return STR;

}

std::vector<SFLD> STRTFLD(std::string strdata){
	std::vector<std::vector<std::string>> STRDATA=TSSV(strdata, ";");
	std::vector<SFLD> FLD;
	for(uint i=0;i<STRDATA.size();i++){
		FLD.push_back(SFLD({std::stof(STRDATA[i][0]),
							  std::stof(STRDATA[i][1]),
							  std::stof(STRDATA[i][2]),
							  std::stof(STRDATA[i][3]),
							  std::stof(STRDATA[i][4])}));
	}
	return FLD;


}

std::vector<std::vector<double>> MINMAX(std::vector<SFLD> field){//Returns min and max values of  m,e of a given field
	assert(field.size()>0);

	std::vector<std::vector<double>> minmax={{field[0].m,field[0].m},{field[0].e,field[0].e}};

		for (uint i=1;i<field.size();i++){
		if(minmax[0][1]<field[i].m){// max m
			minmax[0][1]=field[i].m;
		}
		if(minmax[0][0]>field[i].m){//min m
			minmax[0][0]=field[i].m;
		}
		if(minmax[1][1]<field[i].e){//max e
			minmax[1][1]=field[i].e;
		}
		if(minmax[1][0]>field[i].e){//min e
			minmax[1][0]=field[i].e;
		}
	}

	return minmax;
}

void NRMFLD(std::vector<SFLD> & field){//Normalize e and m field parameters
	assert(field.size()>0);

	double maxm=field[0].m;
	double minm=field[0].m;
	double maxe=field[0].e;
	double mine=field[0].e;

	for (uint i=1;i<field.size();i++){
		if(maxm<field[i].m){
			maxm=field[i].m;
		}
		if(minm>field[i].m){
			minm=field[i].m;
		}
		if(maxe<field[i].e){
			maxe=field[i].e;
		}
		if(mine>field[i].e){
			mine=field[i].e;
		}
	}

	//std::cout<<"EMS.cpp::NRMFLD::"<<"maxm="<<maxm<<" minm="<<minm<<" maxe="<<maxe<<" mine="<<mine<<std::endl;

	for (uint i=0;i<field.size();i++){
		if(maxm==minm)field[i].m=0;
		else field[i].m=(field[i].m-minm)/(maxm-minm);

		if(maxe==mine) field[i].e=0;
		else field[i].e=(field[i].e-mine)/(maxe-mine);
	}


}

void NRMFLD(std::vector<std::vector<SFLD>> &field){//Normalize e and m field parameters
	assert(field.size()>0);

	std::vector<double> maxm;
	std::vector<double> minm;
	std::vector<double> maxe;
	std::vector<double> mine;
	for(uint i=0;i<field[0].size();i++){
		maxm.push_back(field[0][i].m);
		minm.push_back(field[0][i].m);
		maxe.push_back(field[0][i].m);
		mine.push_back(field[0][i].m);
	}

	for (uint i=1;i<field.size();i++){for (uint ii=0;ii<field[i].size();ii++){
		if(maxm[ii]<field[i][ii].m){
			maxm[ii]=field[i][ii].m;
		}
		if(minm[ii]>field[i][ii].m){
			minm[ii]=field[i][ii].m;
		}
		if(maxe[ii]<field[i][ii].e){
			maxe[ii]=field[i][ii].e;
		}
		if(mine[ii]>field[i][ii].e){
			mine[ii]=field[i][ii].e;
		}
	}}

	//std::cout<<"EMS.cpp::NRMFLD::"<<"maxm="<<maxm<<" minm="<<minm<<" maxe="<<maxe<<" mine="<<mine<<std::endl;

	for (uint i=0;i<field.size();i++){{for (uint ii=0;ii<field[i].size();ii++){
		if(maxm[ii]==minm[ii])field[i][ii].m=0;
		else field[i][ii].m=(field[i][ii].m-minm[ii])/(maxm[ii]-minm[ii]);

		if(maxe[ii]==mine[ii]) field[i][ii].e=0;
		else field[i][ii].e=(field[i][ii].e-mine[ii])/(maxe[ii]-mine[ii]);
	}}


}
}

void LOGFLD(std::vector<SFLD> & field){//Normalize e and m field parameters
	for (uint i=0;i<field.size();i++){
		field[i].m=log10(field[i].m+1);
		field[i].e=log10(field[i].e+1);
	}


}


void print(std::vector<SFLD> fld){
	std::cout<<"print(std::vector<SFLD> fld)"<<std::endl;
	std::cout<<"fld.size()="<<fld.size()<<std::endl;

	//Normalization
	std::vector<float> mx={fld[0].m, fld[0].e};
	std::vector<float> mn={fld[0].m, fld[0].e};

		for(int i=1;i<fld.size();i++){

		std::vector<float> val={fld[i].m, fld[i].e};

		for(uint ii=0;ii<mx.size();ii++){

			if(mx[ii]<val[ii]){
				mx[ii]=val[ii];
			}

			if(mn[ii]>val[ii]){
				mn[ii]=val[ii];
			}
		}
	}

	std::cout<<"fld::mx.m="<<mx[0]<<std::endl;
	std::cout<<"fld::mn.m="<<mn[0]<<std::endl;

	std::cout<<"fld::mx.e="<<mx[1]<<std::endl;
	std::cout<<"fld::mn.e="<<mn[1]<<std::endl;

}
