#ifndef EMFS_H_INCLUDED
#define EMFS_H_INCLUDED

//Field forming susceptometer compute rutines

#include <stdlib.h>
#include <vector>
#include <string>
#include <functional>
#include <iostream>
#include <limits>
#include <algorithm>
#include <cmath>
#include <math.h>
#include <cassert>
#include <functional>

#include "../SUBM/FHL/FHL.hpp"
#include "../SUBM/SFCR/SOFT/SFCR.h"

#ifndef VOXEL_GRID
#define VOXEL_GRID
struct VXG{//Voxel grid
	float xgs,ygs,zgs;//Grid size
	float xll, yll, zll;//Grid lower coordinate limit
	float xul, yul, zul;//Grid upper coordinate limit
};
#endif

struct SFLD{//Scalar Field struct
	float x,y,z;//Position
	float m,e; //magnetic and electric scalar values
};

bool operator==(SFLD const& lhs, SFLD const& rhs);

struct VFLD{//VECTOR Field struct
	float x,y,z;//Position
	float mx,my,mz;//Magnetic vector field
	float ex,ey,ez;//Electric vector field
};

class EMEFS{//Electro-magnetic efficacious field simulation
public:

	EMEFS();

	EMEFS(const EMEFS& emefs);//Duplicador de clase

	EMEFS& operator=(const EMEFS& emefs);//Igualador de clase

	// VXG GRD={//Computation grid info
	// .xgs=0.4, .ygs=0.4, GRD.zgs=0.4,//Grid size
	// .xll=-15, .yll=-12.5, .zll=-1.6,//Grid lower coordinate limit
	// .xul=15, .yul=12.5, .zul=5};//Grid upper coordinate limit

	VXG GRD={//Computation grid info
	.xgs=0.5, .ygs=0.5, GRD.zgs=0.5,//Grid size
	.xll=-10, .yll=-10, .zll=-1.6,//Grid lower coordinate limit
	.xul=10, .yul=10, .zul=5};//Grid upper coordinate limit

	std::vector<std::vector<SFLD>> FS;//Fields sources
	std::vector <double> f;//Frequency of field sources
	std::vector <double> w;//Power of field sources
	std::vector <double> h;//Phase of field sources

	std::vector<std::vector<SFLD>> F;//Scalar Fields
	std::vector<SFLD> MPR;//Material properties for field computation/

	std::vector<SFLD> FCA(double sx, double sy, double sz, double a);//Field computation alg
	std::vector<SFLD> EFCA(double sx, double sy, double sz);//Eficacious field computation alg

	void CF();//Copute efficacious sinusoidal field
	int NCS=512;//Number of compute steps
	//int NCS=16;//Number of compute steps
	int CS=0;//Compute step
	void PCF();//Partial Copute efficacious sinusoidal field

	//Post processing
	void FI();//Field interpolation
	SFLD CFI(uint n, uint i);//Compute field interference
	std::vector<SFLD> CFI(uint i);//Compute field interference
	std::vector<std::vector<SFLD>> CFI();//Compute field interference

	std::vector<SFLD> NRM(uint n);//Normalize field
	std::vector<std::vector<SFLD>> NRM();//Normalize field

	std::vector<SFLD> FAC();//Field additive combination// Combines all fields of all sources in a combined field
	//Post processing

};


//////////////////////////////Field transformations

//void FtVA(std::vector<SFLD> field, std::vector<float> & position, std::vector<float> & color ,std::string type ="m");//Field to vertex array
void FtVA(std::vector<SFLD> field, std::vector<float> & position, std::vector<float> & color ,std::string type ="m", float mult=1);//Field to vertex array
//////////////////////////////Field transformations

std::vector <float> SFLDtFV(SFLD sfld);//Scalar field to float vector
SFLD FVtSFLD(std::vector <float> fv);//Float vector to scalar field
std::vector<std::vector<float>> SFLDVtFV(std::vector<SFLD> sfldv);//Scalar field vector to float vector
std::vector <SFLD> FVtSFLDV(std::vector<std::vector<float>> fv);//Scalar field to float vector

std::string FLDTSTR(std::vector<SFLD> ffsfield);
std::vector<SFLD> STRTFLD(std::string strdata);

std::vector<double> EFV(std::vector<SFLD> field, std::string var="m");//Extract field vaiable

uint IDBP(std::vector<SFLD> field, double x, double y, double z);//Returns the id of the closest field element to a given position

bool CFXC(const SFLD &a, const SFLD &b);//Comparison between x coordinates for sorting porpouses
bool CFYC(const SFLD &a, const SFLD &b);//Comparison between y coordinates for sorting porpouses
bool CFZC(const SFLD &a, const SFLD &b);//Comparison between z coordinates for sorting porpouses
bool CFMC(const SFLD &a, const SFLD &b);//Comparison between m values for sorting porpouses
bool CFEC(const SFLD &a, const SFLD &b);//Comparison between e values for sorting porpouses

std::vector<std::vector<double>> MINMAX(std::vector<SFLD> field);//Returns min and max values of  m,e of a given field

void NRMFLD(std::vector<SFLD> &field);//Normalize e and m field parameters
void NRMFLD(std::vector<std::vector<SFLD>> &field);//Normalize e and m field parameters

void LOGFLD(std::vector<SFLD> &field);//Applies a logaritmic function to the field parameters


void print(std::vector<SFLD> fld);

#endif
