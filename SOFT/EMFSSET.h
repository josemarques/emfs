#ifndef EMFSSET_H_INCLUDED
#define EMFSSET_H_INCLUDED

//Field forming susceptometer compute rutines

#include <stdlib.h>
#include <vector>
#include <string>
#include <functional>
#include <iostream>
#include <limits>
#include <algorithm>
#include <cmath>
#include <math.h>
#include <cassert>
#include <functional>

#include "EMFS.h"



class EMEFSSET{//Electro-magnetic efficacious field simulation set. Sets o fimulations sharing material properties and grid
public:

	EMEFSSET();

	EMEFSSET(const EMEFSSET& emefsset);//Duplicador de clase

	EMEFSSET& operator=(const EMEFSSET& emefsset);//Igualador de clase

	// VXG GRD={//Computation grid info
	// .xgs=0.4, .ygs=0.4, GRD.zgs=0.4,//Grid size
	// .xll=-15, .yll=-12.5, .zll=-1.6,//Grid lower coordinate limit
	// .xul=15, .yul=12.5, .zul=5};//Grid upper coordinate limit

	VXG GRD={//Computation grid info
	.xgs=0.2, .ygs=0.2, GRD.zgs=0.2,//Grid size
	.xll=-5, .yll=-5, .zll=-1.6,//Grid lower coordinate limit
	.xul=5, .yul=5, .zul=5};//Grid upper coordinate limit

	std::vector<SFLD> MPR;//Material properties shared on all simulations //TODO std::shared_ptr

	std::vector<EMEFS> SIMS;//Simulation parameters and fields

};


#endif
